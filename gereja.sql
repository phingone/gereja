-- phpMyAdmin SQL Dump
-- version 4.2.12deb2+deb8u2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 12, 2017 at 11:25 PM
-- Server version: 10.0.30-MariaDB-0+deb8u1
-- PHP Version: 5.6.30-0+deb8u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `gereja`
--
CREATE DATABASE IF NOT EXISTS `gereja` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `gereja`;

-- --------------------------------------------------------

--
-- Table structure for table `s_wilayah`
--

DROP TABLE IF EXISTS `s_wilayah`;
CREATE TABLE IF NOT EXISTS `s_wilayah` (
`s_idwilayah` int(11) NOT NULL,
  `s_kodewilayah` varchar(3) NOT NULL,
  `s_namawilayah` varchar(100) NOT NULL,
  `s_keteranganwilayah` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `s_wilayah`
--

INSERT INTO `s_wilayah` (`s_idwilayah`, `s_kodewilayah`, `s_namawilayah`, `s_keteranganwilayah`) VALUES
(1, '001', 'Wilayah 1', 'Keterangan Wilayah 1, test update'),
(2, '002', 'Wilayah 2', 'Keterangan Wilayah 2'),
(4, '003', 'Wilayah 3', 'Keterangan Wilayah 3'),
(5, '004', 'Wilayah 4', 'Keterangan Wilayah 4'),
(6, '005', 'Wilayah 5', 'Keterangan Wilayah 5'),
(7, '006', 'Wilayah 6', 'Keterangan Wilayah 6'),
(8, '007', 'Wilayah 7', 'Keterangan Wilayah 7');

-- --------------------------------------------------------

--
-- Table structure for table `t_member`
--

DROP TABLE IF EXISTS `t_member`;
CREATE TABLE IF NOT EXISTS `t_member` (
`t_idmember` int(11) NOT NULL,
  `t_urutkkmember` int(5) NOT NULL,
  `t_urutakmember` int(5) NOT NULL,
  `t_wilayahmember` int(2) NOT NULL,
  `t_noregistrasimember` varchar(12) NOT NULL,
  `t_namamember` varchar(150) NOT NULL,
  `t_jeniskelaminmember` tinyint(2) NOT NULL,
  `t_hubunganmember` tinyint(2) NOT NULL,
  `t_tempatlahirmember` varchar(150) NOT NULL,
  `t_tanggallahirmember` date NOT NULL,
  `t_alamatjalanmember` text NOT NULL,
  `t_alamatrtmember` varchar(3) NOT NULL,
  `t_alamatrwmember` varchar(3) NOT NULL,
  `t_alamatkelurahanmember` varchar(150) NOT NULL,
  `t_alamatkecamatanmember` varchar(150) NOT NULL,
  `t_alamatkotamember` varchar(150) NOT NULL,
  `t_kodeposmember` varchar(5) NOT NULL,
  `t_telprumahmember` varchar(16) DEFAULT NULL,
  `t_telphpmember` varchar(18) DEFAULT NULL,
  `t_emailmember` varchar(150) DEFAULT NULL,
  `t_pendidikanmember` varchar(150) DEFAULT NULL,
  `t_pekerjaanmember` varchar(150) DEFAULT NULL,
  `t_perusahaanmember` varchar(150) DEFAULT NULL,
  `t_perusahaanalamatmember` text,
  `t_telpperusahaanmember` varchar(15) DEFAULT NULL,
  `t_tempatbaptismember` varchar(150) DEFAULT NULL,
  `t_tanggalbaptismember` date DEFAULT NULL,
  `t_tempatsidimember` varchar(150) DEFAULT NULL,
  `t_tanggalsidimember` date DEFAULT NULL,
  `t_tempatnikahmember` varchar(150) DEFAULT NULL,
  `t_tanggalnikahmember` date DEFAULT NULL,
  `t_keteranganmember` text,
  `t_iskkmember` int(2) NOT NULL DEFAULT '0',
  `t_isoutmember` int(2) NOT NULL DEFAULT '0',
  `t_oldwilayahmember` int(2) DEFAULT NULL,
  `t_oldurutkkmember` int(5) DEFAULT NULL,
  `t_oldurutakmember` int(5) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `s_wilayah`
--
ALTER TABLE `s_wilayah`
 ADD PRIMARY KEY (`s_idwilayah`);

--
-- Indexes for table `t_member`
--
ALTER TABLE `t_member`
 ADD PRIMARY KEY (`t_idmember`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `s_wilayah`
--
ALTER TABLE `s_wilayah`
MODIFY `s_idwilayah` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `t_member`
--
ALTER TABLE `t_member`
MODIFY `t_idmember` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
