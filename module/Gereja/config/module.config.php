<?php
namespace Gereja;

use Zend\Router\Http\Segment;
use Gereja\Controller\Beranda;
use Gereja\Controller\Setting\Wilayah;
use Gereja\Controller\Pendataan\KepalaKeluarga;

return [
    'router' => [
        'routes' => [
            'data_kepala_keluarga' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/data_kepala_keluarga[/:action[/:id]]',
                    'constraints' => [
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+'
                    ],
                    'defaults' => [
                        'controller' => KepalaKeluarga::class,
                        'action' => 'index'
                    ]
                ]
            ],
            'setting_wilayah' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/setting_wilayah[/:action[/:id]]',
                    'constraints' => [
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+'
                    ],
                    'defaults' => [
                        'controller' => Wilayah::class,
                        'action' => 'index'
                    ]
                ]
            ],
            'index' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/index[/:action[/:id]]',
                    'constraints' => [
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+'
                    ],
                    'defaults' => [
                        'controller' => Beranda::class,
                        'action' => 'index'
                    ]
                ]
            ]
        ]
     ],
     'view_manager' => [
         'display_not_found_reason' => true,
         'display_exceptions' => true,
         'doctype' => 'HTML5',
         'not_found_template' => 'error404',
         'exception_template' => 'error/exception',
         'template_map' => [
             'layout' => __DIR__ . '/../view/layout/layout.phtml',
             'menu' => __DIR__ . '/../view/layout/menu.phtml',
             'sidemenu' => __DIR__ . '/../view/layout/sidemenu.phtml',
             'error404' => __DIR__ . '/../view/error/404.phtml',
             'error/exception' => __DIR__ . '/../view/error/index.phtml',
//             'blankTemplate' => __DIR__ . '/../view/layout/empty.phtml'
         ],
         'template_path_stack' => [
             'gereja' => __DIR__ . '/../view'
         ]
     ]
];