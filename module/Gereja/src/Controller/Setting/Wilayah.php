<?php
namespace Gereja\Controller\Setting;

use Gereja\BaseController\BaseController;
use Zend\View\Model\ViewModel;
use Gereja\Form\Setting\FrmWilayah;
use Gereja\Model\Base\Setting\WilayahBase;
use Zend\Json\Json;

class Wilayah extends BaseController
{

    public function indexAction()
    {
        $view = new ViewModel([
            "top_setting" => "active",
            "side_wilayah" => "active",
            "judul_halaman" => "Setting Wilayah",
            "judul_desk" => "Daftar Wilayah"
        ]);
        return $view;
    }
    
    public function datagridAction()
    {
        $model = new WilayahBase();
        $postedData = $this->getRequest()->getPost();
        $model->exchangeArray($postedData);
        if ($model->direction == 2):
        $model->page = $model->page + 1;
        elseif ($model->direction == 1) :
        $model->page = $model->page - 1;
        endif;
        $page = (int) $model->page;
        $limit = (int) $model->rows;
        $RowsCount = $this->wilayahTable->gridCount($model);
        $totalPages = $RowsCount > 0 && $limit > 0 ? ceil($RowsCount / $limit) : 0;
        $page = $page <= $totalPages ? $page : 1;
        $offset = $limit * $page - $limit;
        $model->offset = $offset < 0 ? 0 : $offset;
        $data = $this->wilayahTable->gridData($model);
        $htmlTable = "";
        $counter = 0;
        $classRow = "";
        if ($RowsCount == 0) {
            $htmlTable .= "<tr>";
            $htmlTable .= "<td colspan='3'> Tidak ada data</td>";
            $htmlTable .= "</tr>";
        }else{
            foreach ($data as $row) {
                if($counter % 2 == 0) $classRow ="info";
                else $classRow ="warning";
                $htmlTable .= "<tr class='$classRow'>";
                $htmlTable .= "<td class='text-center'>".$row->s_kodewilayah."</td>";
                $htmlTable .= "<td class='text-left'>".$row->s_namawilayah."</td>";
                $htmlTable .= "<td class='text-center'>";
                $htmlTable .= "<a href='setting_wilayah/edit/" . $row->s_idwilayah . "' class='btn btn-warning' title='Edit'><i class='fa fa-fw fa-edit'></i></a> ";
                $htmlTable .= "<a href='#' class='btn btn-danger' title='Hapus' onclick='hapus(" . $row->s_idwilayah . ");return false;'><i class='fa fa-fw fa-eraser'></i></a> ";
                $htmlTable .= "</td>";
                $htmlTable .= "</tr>";
                $counter++;
            }
        }
        $page = $page == 0 ? 1 : $page;
        $halaman = $RowsCount / (int) $model->rows;
        if(intval($halaman) <= 0){
            $halaman = 1;
        }
        $dataToRender = [
            'HtmlTable' => $htmlTable,
            'HtmlRowsCount' => $RowsCount,
            'HtmlTablePage' => $page,
            'HtmlPage' => $halaman
        ];
        return $this->getResponse()->setContent(Json::encode($dataToRender));
    }
    
    public function tambahAction()
    {
        $frm = new FrmWilayah();
        $request = $this->getRequest();
        if($request->isPost()){
            $model = new WilayahBase();
            $frm->setInputFilter($model->getInputFilter());
            $frm->setData($request->getPost());
            if($frm->isValid()){
                $model->exchangeArray($frm->getData());
                $checkexist = $this->wilayahTable->countByKode($model);
                if($checkexist == 0){
                    $this->wilayahTable->save($model);
                    return $this->redirect()->toRoute('setting_wilayah');
                }else{
                    $frm->get('s_kodewilayah')->setMessages([
                        'Error' => 'Kode ini sudah pernah dipakai'
                    ]);
                }
            }
        }
        $view = new ViewModel([
            "top_setting" => "active",
            "side_wilayah" => "active",
            "judul_halaman" => "Setting Wilayah",
            "judul_desk" => "Tambah Wilayah",
            "frm" => $frm
        ]);
        return $view;
    }
    
    public function editAction()
    {
        $model = new WilayahBase();
        $frm = new FrmWilayah();
        $model->s_idwilayah = $this->params('id');
        $data = $this->wilayahTable->getById($model);
        if(!is_null($data)){
            $frm->bind($data);
        }
        $request = $this->getRequest();
        if($request->isPost()){
            $model = new WilayahBase();
            $frm->setInputFilter($model->getInputFilter());
            $frm->setData($request->getPost());
            if($frm->isValid()){
                $model->exchangeArray($frm->getData());
                $this->wilayahTable->ubah($model);
                return $this->redirect()->toRoute('setting_wilayah');                
            }
        }
        $view = new ViewModel([
            "top_setting" => "active",
            "side_wilayah" => "active",
            "judul_halaman" => "Setting Wilayah",
            "judul_desk" => "Tambah Wilayah",
            "frm" => $frm
        ]);
        return $view;
    }
    
    public function hapusAction()
    {
        $model = new WilayahBase();
        $model->s_idwilayah = $this->getRequest()->getPost('id');
        $this->wilayahTable->hapus($model);
        return $this->getResponse()->setContent('Data berhasil dihapus');
    }
}