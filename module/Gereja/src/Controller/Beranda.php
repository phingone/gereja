<?php
namespace Gereja\Controller;

use Gereja\BaseController\BaseController;
use Zend\View\Model\ViewModel;

class Beranda extends BaseController
{

    public function indexAction()
    {
        $view = new ViewModel([
            "top_beranda" => "active",
            "judul_halaman" => "Beranda",
            "judul_desk" => ""
        ]);
        return $view;
    }
    
    public function pendataanAction()
    {
        $view = new ViewModel([
            "top_pendataan" => "active",            
            "judul_halaman" => "Pendataan",
            "judul_desk" => "Kepala Keluarga & Anggota Keluarga"
        ]);
        return $view;
    }

    public function settingAction()
    {
        $view = new ViewModel([
            "top_setting" => "active",
            "judul_halaman" => "Setting",
            "judul_desk" => ""
        ]);
        return $view;
    }
}