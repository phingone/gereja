<?php
namespace Gereja\Controller\Pendataan;

use Gereja\BaseController\BaseController;
use Zend\View\Model\ViewModel;
use Gereja\Form\Pendataan\FrmKepalaKeluarga;
use Gereja\Model\Base\Pendataan\KepalaKeluargaBase;
use Zend\Json\Json;
use Gereja\Model\Base\Setting\WilayahBase;

class KepalaKeluarga extends BaseController 
{
    public function indexAction()
    {
        $view = new ViewModel([
            "top_pendataan" => "active",
            "side_kepalakeluarga" => "active",
            "judul_halaman" => "Pendataan",
            "judul_desk" => "Daftar Kepala Keluarga"
        ]);
        return $view;
    }
    
    public function generateregistrasiAction()
    {
        $model = new KepalaKeluargaBase();
        $modelWilayah = new WilayahBase();
        $postedData = $this->getRequest()->getPost();
        $model->exchangeArray($postedData);
        $modelWilayah->s_idwilayah = $model->t_wilayahmember;
        $data = $this->kepalakeluargaTable->getlasturutkk($model);
        $dataWilayah = $this->wilayahTable->getById($modelWilayah);
        $data->t_urutkkmember = (int) $data->t_urutkkmember + 1;
        
        $outputData = [
            "t_urutkkmember" => $data->t_urutkkmember,
            "t_urutakmember" => 1,
            "t_noregistrasimember" => $dataWilayah->s_kodewilayah . str_pad($data->t_urutkkmember, 3,'0',STR_PAD_LEFT).str_pad(1, 3,'0',STR_PAD_LEFT)
        ];
        return $this->getResponse()->setContent(Json::encode($outputData));
    }
    
    public function datagridAction()
    {
        $model = new KepalaKeluargaBase();
        $postedData = $this->getRequest()->getPost();
        $model->exchangeArray($postedData);
        if ($model->direction == 2):
        $model->page = $model->page + 1;
        elseif ($model->direction == 1) :
        $model->page = $model->page - 1;
        endif;
        $page = (int) $model->page;
        $limit = (int) $model->rows;
        $RowsCount = $this->kepalakeluargaTable->gridCount($model);
        $totalPages = $RowsCount > 0 && $limit > 0 ? ceil($RowsCount / $limit) : 0;
        $page = $page <= $totalPages ? $page : 1;
        $offset = $limit * $page - $limit;
        $model->offset = $offset < 0 ? 0 : $offset;
        $data = $this->kepalakeluargaTable->gridData($model);
        $htmlTable = "";
        $counter = 0;
        $classRow = "";
        if ($RowsCount == 0) {
            $htmlTable .= "<tr>";
            $htmlTable .= "<td colspan='4'> Tidak ada data</td>";
            $htmlTable .= "</tr>";
        }else{
            foreach ($data as $row) {
                if($counter % 2 == 0) $classRow ="info";
                else $classRow ="warning";
                
                $alamat = $row->t_alamatjalanmember;
                if(!is_null($row->t_alamatrtmember)){
                    $alamat = $alamat .", Rt. ".str_pad($row->t_alamatrtmember, 3,0,STR_PAD_LEFT);
                }
                if(!is_null($row->t_alamatrwmember)){
                    $alamat = $alamat .", Rw. ".str_pad($row->t_alamatrwmember, 3,0,STR_PAD_LEFT);
                }
                if(!is_null($row->t_alamatkelurahanmember)){
                    $alamat = $alamat .", Kelurahan ".$row->t_alamatkelurahanmember;
                }
                if(!is_null($row->t_alamatkecamatanmember)){
                    $alamat = $alamat .", Kecamatan ".$row->t_alamatkecamatanmember;
                }
                if(!is_null($row->t_alamatkotamember)){
                    $alamat = $alamat .", ".$row->t_alamatkotamember;
                }
                if(!is_null($row->t_kodeposmember)){
                    $alamat = $alamat .", Kodepos ".$row->t_kodeposmember;
                }
                if(!is_null($row->t_telprumahmember)){
                    $alamat = $alamat .", Telp. ".$row->t_telprumahmember;
                }
                if(!is_null($row->t_telphpmember)){
                    $alamat = $alamat .", Hp. ".$row->t_telphpmember;
                }
                
                $htmlTable .= "<tr class='$classRow'>";
                $htmlTable .= "<td class='text-center'>".$row->t_noregistrasimember."</td>";
                $htmlTable .= "<td class='text-left'>".$row->t_namamember."</td>";
                $htmlTable .= "<td class='text-left'>".$alamat."</td>";
                $htmlTable .= "<td class='text-center'>";
                $htmlTable .= "<a href='data_kepala_keluarga/edit/" . $row->t_idmember . "' class='btn btn-info' title='Tambah Anggota Keluarga'><i class='fa fa-fw fa-plus'></i></a> ";
                $htmlTable .= "<a href='data_kepala_keluarga/edit/" . $row->t_idmember . "' class='btn btn-warning' title='Edit Kepala Keluarga'><i class='fa fa-fw fa-edit'></i></a> ";
                $htmlTable .= "<a href='#' class='btn btn-danger' title='Hapus Kepala Keluarga' onclick='hapus(" . $row->t_idmember . ");return false;'><i class='fa fa-fw fa-eraser'></i></a> ";
                $htmlTable .= "</td>";
                $htmlTable .= "</tr>";
                $counter++;
            }
        }
        $page = $page == 0 ? 1 : $page;
        $halaman = $RowsCount / (int) $model->rows;
        if(intval($halaman) <= 0){
            $halaman = 1;
        }
        $dataToRender = [
            'HtmlTable' => $htmlTable,
            'HtmlRowsCount' => $RowsCount,
            'HtmlTablePage' => $page,
            'HtmlPage' => $halaman
        ];
        return $this->getResponse()->setContent(Json::encode($dataToRender));
    }
    
    public function tambahAction()
    {
        $frm = new FrmKepalaKeluarga($this->wilayahTable->comboWilayah());
        $req = $this->getRequest();
        if($req->isPost()){
            $model = new KepalaKeluargaBase();
            $frm->setInputFilter($model->getInputFilter());
            $frm->setData($req->getPost());
            if($frm->isValid()){
                $model->exchangeArray($frm->getData());
                $this->kepalakeluargaTable->simpan($model);
                return $this->redirect()->toRoute('data_kepala_keluarga');
            }
        }
        $view = new ViewModel([
            "top_pendataan" => "active",
            "side_kepalakeluarga" => "active",
            "judul_halaman" => "Pendataan",
            "judul_desk" => "Tambah Kepala Keluarga",
            "frm" => $frm
        ]);
        return $view;
    }
    
    public function editAction()
    {
        $req = $this->getRequest();
        $model = new KepalaKeluargaBase();        
        $frm = new FrmKepalaKeluarga($this->wilayahTable->comboWilayah());
        $model->t_idmember = $this->params('id');
        $data = $this->kepalakeluargaTable->getid($model);
        if(!is_null($data)){
            $data->t_tanggallahirmember = date('d-m-Y',strtotime($data->t_tanggallahirmember));
            $data->t_tanggalbaptismember = date('d-m-Y',strtotime($data->t_tanggalbaptismember));
            $data->t_tanggalsidimember = date('d-m-Y',strtotime($data->t_tanggalsidimember));
            $data->t_tanggalnikahmember = date('d-m-Y',strtotime($data->t_tanggalnikahmember));
            $frm->bind($data);
        }
        if($req->isPost()){            
            $frm->setInputFilter($model->getInputFilter());
            $frm->setData($req->getPost());
            if($frm->isValid()){
                $model->exchangeArray($frm->getData());
                $this->kepalakeluargaTable->ubah($model);
                return $this->redirect()->toRoute('data_kepala_keluarga');
            }
        }
        $view = new ViewModel([
            "top_pendataan" => "active",
            "side_kepalakeluarga" => "active",
            "judul_halaman" => "Pendataan",
            "judul_desk" => "Tambah Kepala Keluarga",
            "frm" => $frm
        ]);
        return $view;
    }
    
    public function hapusAction()
    {
        $model = new KepalaKeluargaBase();
        $model->t_idmember = $this->getRequest()->getPost('id');
        $this->kepalakeluargaTable->hapus($model);
        return $this->getResponse()->setContent('Data berhasil dihapus');
    }
}