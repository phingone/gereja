<?php
namespace Gereja;

use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Gereja\Controller\Beranda;
use Zend\ServiceManager\ServiceManager;
use Zend\ModuleManager\ModuleManager;
use Gereja\Controller\Setting\Wilayah;
use Zend\Db\Adapter\AdapterInterface;
use Zend\Db\ResultSet\ResultSet;
use Gereja\Model\Base\Setting\WilayahBase;
use Gereja\Model\Tables\Setting\WilayahTable;
use Zend\Db\TableGateway\TableGateway;
use Gereja\Controller\Pendataan\KepalaKeluarga;
use Gereja\Model\Tables\Pendataan\KepalaKeluargaTable;
use Gereja\Model\Base\Pendataan\KepalaKeluargaBase;

class Module implements ConfigProviderInterface
{

    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }

    public function init(ModuleManager $moduleManager)
    {
        $SharedManager = $moduleManager->getEventManager()->getSharedManager();
        $SharedManager->attach(__NAMESPACE__, 'dispatch', function ($event){
            $event->getTarget()->layout('layout');
        });
    }

    public function getControllerConfig()
    {
        return [
            'factories' => [
                KepalaKeluarga::class => function (ServiceManager $sm) {
                    return new KepalaKeluarga(
                        NULL,
                        $sm->get(WilayahTable::class),
                        $sm->get(KepalaKeluargaTable::class));
                },
                Wilayah::class => function (ServiceManager $sm){
                    return new Wilayah(
                        NULL,
                        $sm->get(WilayahTable::class));  
                },
                Beranda::class => function (ServiceManager $sm) {
                    return new Beranda();
                }
            ]
        ];
    }
    
    public function getServiceConfig()
    {
        return [
            'factories' => [
                'WilayahTable' => function (ServiceManager $sm) {
                    $dbAdapter = $sm->get(AdapterInterface::class);
                    $resultSet = new ResultSet();
                    $resultSet->setArrayObjectPrototype(new WilayahBase());
                    return new TableGateway('s_wilayah',$dbAdapter, NULL,$resultSet);
                },
                WilayahTable::class => function (ServiceManager $sm) {
                    return new WilayahTable($sm->get('WilayahTable'));
                },
                'KepalaKeluargaTable' => function (ServiceManager $sm) {
                    $dbAdapter = $sm->get(AdapterInterface::class);
                    $resultSet = new ResultSet();
                    $resultSet->setArrayObjectPrototype(new KepalaKeluargaBase());
                    return new TableGateway('t_member',$dbAdapter, NULL,$resultSet);
                },
                KepalaKeluargaTable::class => function (ServiceManager $sm) {
                    return new KepalaKeluargaTable($sm->get('KepalaKeluargaTable'));  
                }
            ]
        ];
    }
}