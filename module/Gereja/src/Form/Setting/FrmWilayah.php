<?php
namespace Gereja\Form\Setting;

use Zend\Form\Form;
use Zend\Form\Element\Text;
use Zend\Form\Element\Textarea;

class FrmWilayah extends Form
{

    public function __construct()
    {
        parent::__construct();
        
        $this->add([
            'name' => 's_idwilayah',
            'type' => Text::class,                        
        ]);
        
        $this->add([
            'name' => 's_kodewilayah',
            'type' => Text::class,
            'options' => [
                'label' => 'Kode Wilayah',
                'label_attributes' => [
                    'class' => 'label-control col-sm-2',
                    'style' => 'height:35px;padding-top:5px;'
                ]
            ],
            'attributes' => [
                'id' => 's_kodewilayah',
                'class' => 'form-control',                
                'maxlength' => 3
            ]
        ]);
        
        $this->add([
            'name' => 's_namawilayah',
            'type' => Text::class,
            'options' => [
                'label' => 'Nama Wilayah',
                'label_attributes' => [
                    'class' => 'label-control col-sm-2',
                    'style' => 'height:35px;padding-top:5px;'
                ]
            ],
            'attributes' => [
                'id' => 's_namawilayah',
                'class' => 'form-control',                
            ]
        ]);
        
        $this->add([
            'name' => 's_keteranganwilayah',
            'type' => Textarea::class,
            'options' => [
                'label' => 'Keterangan Wilayah',
                'label_attributes' => [
                    'class' => 'label-control col-sm-2',
                    'style' => 'height:35px;padding-top:5px;'
                ]
            ],
            'attributes' => [
                'id' => 's_keteranganwilayah',
                'class' => 'form-control',
            ]
        ]);
    }
}