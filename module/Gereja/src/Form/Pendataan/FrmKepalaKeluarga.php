<?php
namespace Gereja\Form\Pendataan;

use Zend\Form\Form;
use Zend\Form\Element\Hidden;
use Zend\Form\Element\Text;
use Zend\Form\Element\Select;
use Zend\Form\Element\Textarea;
use Zend\Form\Element\Email;

class FrmKepalaKeluarga extends Form 
{
    public function __construct($Ws = Null)
    {
        parent::__construct();
        
        $this->add([
            'name' => 't_idmember',
            'type' => Hidden::class,
        ]);
        
        $this->add([
            'name' => 't_urutkkmember',
            'type' => Hidden::class,
            'attributes' => [
                'id' => 't_urutkkmember',                
            ]
        ]);
        
        $this->add([
            'name' => 't_urutakmember',
            'type' => Hidden::class,
            'attributes' => [
                'id' => 't_urutakmember',
            ]
        ]);
        
        $this->add([
            'name' => 't_wilayahmember',
            'type' => Select::class,
            'options' => [
                'label' => 'Wilayah',
                'label_attributes' => [
                    'class' => 'label-control col-sm-4',
                    'style' => 'padding-top:5px;'
                ],
                'empty_option' => 'Pilih salah satu', 
                'value_options' => $Ws
            ],
            'attributes' => [
                'id' => 't_wilayahmember',
                'class' => 'form-control',
            ]
        ]);
        
        $this->add([
            'name' => 't_noregistrasimember',
            'type' => Text::class,
            'options' => [
                'label' => 'Nomor Registrasi',
                'label_attributes' => [
                    'class' => 'label-control col-sm-4',
                    'style' => 'padding-top:5px;'
                ]
            ],
            'attributes' => [
                'id' => 't_noregistrasimember',
                'class' => 'form-control',                
            ]
        ]);
        
        $this->add([
            'name' => 't_namamember',
            'type' => Text::class,
            'options' => [
                'label' => 'Nama',
                'label_attributes' => [
                    'class' => 'label-control col-sm-4',
                    'style' => 'padding-top:5px;'
                ]
            ],
            'attributes' => [
                'id' => 't_namamember',
                'class' => 'form-control',
            ]
        ]);        
        
        $this->add([
            'name' => 't_jeniskelaminmember',
            'type' => Select::class,
            'options' => [
                'label' => 'Jenis Kelamin',
                'label_attributes' => [
                    'class' => 'label-control col-sm-4',
                    'style' => 'padding-top:5px;'
                ],
                'empty_option' => 'Pilih salah satu',
                'value_options' => [
                    '1' => 'Laki-laki',
                    '2' => 'Perempuan'
                ]
            ],
            'attributes' => [
                'id' => 't_jeniskelaminmember',
                'class' => 'form-control',
            ]
        ]);
        
        $this->add([
            'name' => 't_hubunganmember',
            'type' => Select::class,
            'options' => [
                'label' => 'Hubungan dalam Keluarga',
                'label_attributes' => [
                    'class' => 'label-control col-sm-4',
                    'style' => 'padding-top:5px;'
                ],
                'empty_option' => 'Pilih salah satu',
                'value_options' => [
                    '1' => 'Suami',
                    '2' => 'Istri',
                    '3' => 'Anak',
                    '4' => 'Sepupu',
                    '5' => 'Keponakan',
                    '6' => 'Mertua',
                    '7' => 'Menantu'
                ]
            ],
            'attributes' => [
                'id' => 't_hubunganmember',
                'class' => 'form-control',
            ]
        ]);
        
        $this->add([
            'name' => 't_tempatlahirmember',
            'type' => Text::class,
            'options' => [
                'label' => 'Tempat Lahir',
                'label_attributes' => [
                    'class' => 'label-control col-sm-4',
                    'style' => 'padding-top:5px;'
                ]
            ],
            'attributes' => [
                'id' => 't_tempatlahirmember',
                'class' => 'form-control',
            ]
        ]);
        
        $this->add([
            'name' => 't_tanggallahirmember',
            'type' => Text::class,
            'options' => [
                'label' => 'Tanggal Lahir',
                'label_attributes' => [
                    'class' => 'label-control col-sm-4',
                    'style' => 'padding-top:5px;'
                ]
            ],
            'attributes' => [
                'id' => 't_tanggallahirmember',
                'class' => 'form-control datepicker',
            ]
        ]);
        
        $this->add([
            'name' => 't_alamatjalanmember',
            'type' => Textarea::class,
            'options' => [
                'label' => 'Alamat',
                'label_attributes' => [
                    'class' => 'label-control col-sm-4',
                    'style' => 'padding-top:5px;'
                ]
            ],
            'attributes' => [
                'id' => 't_alamatjalanmember',
                'class' => 'form-control',
            ]
        ]);
        
        $this->add([
            'name' => 't_alamatrtmember',
            'type' => Text::class,
            'options' => [
                'label' => 'RT',
                'label_attributes' => [
                    'class' => 'label-control col-sm-4',
                    'style' => 'padding-top:5px;'
                ]
            ],
            'attributes' => [
                'id' => 't_alamatrtmember',
                'class' => 'form-control',
            ]
        ]);
        
        $this->add([
            'name' => 't_alamatrwmember',
            'type' => Text::class,
            'options' => [
                'label' => 'RW',
                'label_attributes' => [
                    'class' => 'label-control col-sm-2',
                    'style' => 'padding-top:5px;'
                ]
            ],
            'attributes' => [
                'id' => 't_alamatrwmember',
                'class' => 'form-control',
            ]
        ]);
        
        $this->add([
            'name' => 't_alamatkelurahanmember',
            'type' => Text::class,
            'options' => [
                'label' => 'Kelurahan',
                'label_attributes' => [
                    'class' => 'label-control col-sm-4',
                    'style' => 'padding-top:5px;'
                ]
            ],
            'attributes' => [
                'id' => 't_alamatkelurahanmember',
                'class' => 'form-control',
            ]
        ]);
        
        $this->add([
            'name' => 't_alamatkecamatanmember',
            'type' => Text::class,
            'options' => [
                'label' => 'Kecamatan',
                'label_attributes' => [
                    'class' => 'label-control col-sm-4',
                    'style' => 'padding-top:5px;'
                ]
            ],
            'attributes' => [
                'id' => 't_alamatkecamatanmember',
                'class' => 'form-control',
            ]
        ]);
        
        $this->add([
            'name' => 't_alamatkotamember',
            'type' => Text::class,
            'options' => [
                'label' => 'Kota',
                'label_attributes' => [
                    'class' => 'label-control col-sm-4',
                    'style' => 'padding-top:5px;'
                ]
            ],
            'attributes' => [
                'id' => 't_alamatkotamember',
                'class' => 'form-control',
            ]
        ]);
        
        $this->add([
            'name' => 't_kodeposmember',
            'type' => Text::class,
            'options' => [
                'label' => 'Kodepos',
                'label_attributes' => [
                    'class' => 'label-control col-sm-4',
                    'style' => 'padding-top:5px;'
                ]
            ],
            'attributes' => [
                'id' => 't_kodeposmember',
                'class' => 'form-control',
                'maxlength' => 5
            ]
        ]);
        
        $this->add([
            'name' => 't_telprumahmember',
            'type' => Text::class,
            'options' => [
                'label' => 'Telpon Rumah',
                'label_attributes' => [
                    'class' => 'label-control col-sm-4',
                    'style' => 'padding-top:5px;'
                ]
            ],
            'attributes' => [
                'id' => 't_telprumahmember',
                'class' => 'form-control',
                'maxlength' => 16
            ]
        ]);
        
        $this->add([
            'name' => 't_telphpmember',
            'type' => Text::class,
            'options' => [
                'label' => 'Telpon Handphone',
                'label_attributes' => [
                    'class' => 'label-control col-sm-4',
                    'style' => 'padding-top:5px;'
                ]
            ],
            'attributes' => [
                'id' => 't_telphpmember',
                'class' => 'form-control',
                'maxlength' => 16
            ]
        ]);
        
        $this->add([
            'name' => 't_emailmember',
            'type' => Email::class,
            'options' => [
                'label' => 'Email',
                'label_attributes' => [
                    'class' => 'label-control col-sm-4',
                    'style' => 'padding-top:5px;'
                ]
            ],
            'attributes' => [
                'id' => 't_emailmember',
                'class' => 'form-control'
            ]
        ]);
        
        $this->add([
            'name' => 't_pendidikanmember',
            'type' => Text::class,
            'options' => [
                'label' => 'Pendidikan Terakhir',
                'label_attributes' => [
                    'class' => 'label-control col-sm-4',
                    'style' => 'padding-top:5px;'
                ]
            ],
            'attributes' => [
                'id' => 't_pendidikanmember',
                'class' => 'form-control',
                'maxlength' => 100
            ]
        ]);
        
        $this->add([
            'name' => 't_pekerjaaanmember',
            'type' => Text::class,
            'options' => [
                'label' => 'Pekerjaan',
                'label_attributes' => [
                    'class' => 'label-control col-sm-4',
                    'style' => 'padding-top:5px;'
                ]
            ],
            'attributes' => [
                'id' => 't_pekerjaaanmember',
                'class' => 'form-control',
                'maxlength' => 100
            ]
        ]);
        
        $this->add([
            'name' => 't_perusahaanmember',
            'type' => Text::class,
            'options' => [
                'label' => 'Perusahaan/Tempat Bekerja',
                'label_attributes' => [
                    'class' => 'label-control col-sm-4',
                    'style' => 'padding-top:5px;'
                ]
            ],
            'attributes' => [
                'id' => 't_perusahaanmember',
                'class' => 'form-control',
                'maxlength' => 200
            ]
        ]);
        
        $this->add([
            'name' => 't_perusahaanalamatmember',
            'type' => Textarea::class,
            'options' => [
                'label' => 'Alamat Perusahaan/Tempat Bekerja',
                'label_attributes' => [
                    'class' => 'label-control col-sm-4',
                    'style' => 'padding-top:5px;'
                ]
            ],
            'attributes' => [
                'id' => 't_perusahaanalamatmember',
                'class' => 'form-control',
            ]
        ]);
        
        $this->add([
            'name' => 't_telpperusahaanmember',
            'type' => Text::class,
            'options' => [
                'label' => 'No Telp. Perusahaan/Tempat Bekerja',
                'label_attributes' => [
                    'class' => 'label-control col-sm-4',
                    'style' => 'padding-top:5px;'
                ]
            ],
            'attributes' => [
                'id' => 't_telpperusahaanmember',
                'class' => 'form-control',
                'maxlength' => 16
            ]
        ]);
        
        $this->add([
            'name' => 't_tempatbaptismember',
            'type' => Text::class,
            'options' => [
                'label' => 'Tempat Baptis',
                'label_attributes' => [
                    'class' => 'label-control col-sm-4',
                    'style' => 'padding-top:5px;'
                ]
            ],
            'attributes' => [
                'id' => 't_tempatbaptismember',
                'class' => 'form-control',
                'maxlength' => 200
            ]
        ]);
        
        $this->add([
            'name' => 't_tanggalbaptismember',
            'type' => Text::class,
            'options' => [
                'label' => 'Tanggal Baptis',
                'label_attributes' => [
                    'class' => 'label-control col-sm-4',
                    'style' => 'padding-top:5px;'
                ]
            ],
            'attributes' => [
                'id' => 't_tanggalbaptismember',
                'class' => 'form-control datepicker',
            ]
        ]);
        
        $this->add([
            'name' => 't_tempatsidimember',
            'type' => Text::class,
            'options' => [
                'label' => 'Tempat SIDI',
                'label_attributes' => [
                    'class' => 'label-control col-sm-4',
                    'style' => 'padding-top:5px;'
                ]
            ],
            'attributes' => [
                'id' => 't_tempatsidimember',
                'class' => 'form-control',
                'maxlength' => 200
            ]
        ]);
        
        $this->add([
            'name' => 't_tanggalsidimember',
            'type' => Text::class,
            'options' => [
                'label' => 'Tanggal SIDI',
                'label_attributes' => [
                    'class' => 'label-control col-sm-4',
                    'style' => 'padding-top:5px;'
                ]
            ],
            'attributes' => [
                'id' => 't_tanggalsidimember',
                'class' => 'form-control datepicker',
            ]
        ]);
        
        $this->add([
            'name' => 't_tempatnikahmember',
            'type' => Text::class,
            'options' => [
                'label' => 'Tempat Nikah',
                'label_attributes' => [
                    'class' => 'label-control col-sm-4',
                    'style' => 'padding-top:5px;'
                ]
            ],
            'attributes' => [
                'id' => 't_tempatnikahmember',
                'class' => 'form-control',
                'maxlength' => 200
            ]
        ]);
        
        $this->add([
            'name' => 't_tanggalnikahmember',
            'type' => Text::class,
            'options' => [
                'label' => 'Tanggal Nikah',
                'label_attributes' => [
                    'class' => 'label-control col-sm-4',
                    'style' => 'padding-top:5px;'
                ]
            ],
            'attributes' => [
                'id' => 't_tanggalnikahmember',
                'class' => 'form-control datepicker',
            ]
        ]);
        
        $this->add([
            'name' => 't_keteranganmember',
            'type' => Textarea::class,
            'options' => [
                'label' => 'Keterangan Lainnya',
                'label_attributes' => [
                    'class' => 'label-control col-sm-4',
                    'style' => 'padding-top:5px;'
                ]
            ],
            'attributes' => [
                'id' => 't_keteranganmember',
                'class' => 'form-control',
            ]
        ]);
    }
}