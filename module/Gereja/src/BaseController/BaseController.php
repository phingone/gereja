<?php
namespace Gereja\BaseController;

use Zend\Mvc\Controller\AbstractActionController;
use Gereja\Model\Tables\Setting\WilayahTable;
use Gereja\Model\Tables\Pendataan\KepalaKeluargaTable;

class BaseController extends AbstractActionController
{
    public $Config;
    public $wilayahTable;
    public $kepalakeluargaTable;
    
    public function __construct(array $config = NULL, WilayahTable $wilayahTable = NULL, KepalaKeluargaTable $kepalakeluargaTable = NULL){
        $this->Config = $config;
        $this->wilayahTable = $wilayahTable;
        $this->kepalakeluargaTable = $kepalakeluargaTable;
    }
}