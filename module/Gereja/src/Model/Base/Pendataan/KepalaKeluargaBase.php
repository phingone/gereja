<?php
namespace Gereja\Model\Base\Pendataan;

use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\InputFilter\InputFilter;
use Zend\Validator\StringLength;
use Zend\Validator\Date;
use Zend\Validator\Digits;
use Zend\Validator\EmailAddress;

class KepalaKeluargaBase implements InputFilterAwareInterface {
    
    public $t_idmember;
    public $t_urutkkmember;
    public $t_urutakmember;
    public $t_wilayahmember;
    public $t_noregistrasimember;
    public $t_namamember;
    public $t_jeniskelaminmember;
    public $t_hubunganmember;
    public $t_tempatlahirmember;
    public $t_tanggallahirmember;
    public $t_alamatjalanmember;
    public $t_alamatrtmember;
    public $t_alamatrwmember;
    public $t_alamatkelurahanmember;
    public $t_alamatkecamatanmember;
    public $t_alamatkotamember;
    public $t_kodeposmember;
    public $t_telprumahmember;
    public $t_telphpmember;
    public $t_emailmember;
    public $t_pendidikanmember;
    public $t_pekerjaaanmember;
    public $t_perusahaanmember;
    public $t_perusahaanalamatmember;
    public $t_telpperusahaanmember;
    public $t_tempatbaptismember;
    public $t_tanggalbaptismember;
    public $t_tempatsidimember;
    public $t_tanggalsidimember;
    public $t_tempatnikahmember;
    public $t_tanggalnikahmember;
    public $t_keteranganmember;
    public $t_iskkmember;
    public $t_isoutmember;
    public $t_oldwilayahmember;
    public $t_oldurutkkmember;
    public $t_oldurutakmember;
    
    protected  $inputFilter;
    
    public $direction;
    public $page;
    public $rows;
    public $offset;
    
    public function exchangeArray($data)
    {
        $this->direction = ! empty($data['direction']) ? $data['direction'] : NULL;
        $this->page = ! empty($data['page']) ? $data['page'] : NULL;
        $this->rows = ! empty($data['rows']) ? $data['rows'] : NULL;
        $this->offset = ! empty($data['offset']) ? $data['offset'] : NULL;
        
        $this->t_oldurutakmember = !empty($data["t_oldurutakmember"]) ? $data["t_oldurutakmember"] : null;
        $this->t_oldurutkkmember = !empty($data["t_oldurutkkmember"]) ? $data["t_oldurutkkmember"] : null;
        $this->t_oldwilayahmember = !empty($data["t_oldwilayahmember"]) ? $data["t_oldwilayahmember"] : null;
        $this->t_isoutmember = !empty($data["t_isoutmember"]) ? $data["t_isoutmember"] : null;
        $this->t_iskkmember = !empty($data["t_iskkmember"]) ? $data["t_iskkmember"] : null;
        $this->t_keteranganmember = !empty($data["t_keteranganmember"]) ? $data["t_keteranganmember"] : null;
        $this->t_tanggalnikahmember = !empty($data["t_tanggalnikahmember"]) ? $data["t_tanggalnikahmember"] : null;
        $this->t_tempatnikahmember = !empty($data["t_tempatnikahmember"]) ? $data["t_tempatnikahmember"] : null;
        $this->t_tanggalsidimember = !empty($data["t_tanggalsidimember"]) ? $data["t_tanggalsidimember"] : null;
        $this->t_tempatsidimember = !empty($data["t_tempatsidimember"]) ? $data["t_tempatsidimember"] : null;
        $this->t_tanggalbaptismember = !empty($data["t_tanggalbaptismember"]) ? $data["t_tanggalbaptismember"] : null;
        $this->t_tempatbaptismember = !empty($data["t_tempatbaptismember"]) ? $data["t_tempatbaptismember"] : null;
        $this->t_telpperusahaanmember = !empty($data["t_telpperusahaanmember"]) ? $data["t_telpperusahaanmember"] : null;
        $this->t_perusahaanalamatmember = !empty($data["t_perusahaanalamatmember"]) ? $data["t_perusahaanalamatmember"] : null;
        $this->t_perusahaanmember = !empty($data["t_perusahaanmember"]) ? $data["t_perusahaanmember"] : null;
        $this->t_pekerjaaanmember = !empty($data["t_pekerjaaanmember"]) ? $data["t_pekerjaaanmember"] : null;
        $this->t_pendidikanmember = !empty($data["t_pendidikanmember"]) ? $data["t_pendidikanmember"] : null;
        $this->t_emailmember = !empty($data["t_emailmember"]) ? $data["t_emailmember"] : null;
        $this->t_telphpmember = !empty($data["t_telphpmember"]) ? $data["t_telphpmember"] : null;
        $this->t_telprumahmember = !empty($data["t_telprumahmember"]) ? $data["t_telprumahmember"] : null;
        $this->t_kodeposmember = !empty($data["t_kodeposmember"]) ? $data["t_kodeposmember"] : null;
        $this->t_alamatkotamember = !empty($data["t_alamatkotamember"]) ? $data["t_alamatkotamember"] : null;
        $this->t_alamatkecamatanmember = !empty($data["t_alamatkecamatanmember"]) ? $data["t_alamatkecamatanmember"] : null;
        $this->t_alamatkelurahanmember = !empty($data["t_alamatkelurahanmember"]) ? $data["t_alamatkelurahanmember"] : null;
        $this->t_alamatrwmember = !empty($data["t_alamatrwmember"]) ? $data["t_alamatrwmember"] : null;
        $this->t_alamatrtmember = !empty($data["t_alamatrtmember"]) ? $data["t_alamatrtmember"] : null;
        $this->t_alamatjalanmember = !empty($data["t_alamatjalanmember"]) ? $data["t_alamatjalanmember"] : null;
        $this->t_tanggallahirmember = !empty($data["t_tanggallahirmember"]) ? $data["t_tanggallahirmember"] : null;
        $this->t_tempatlahirmember = !empty($data["t_tempatlahirmember"]) ? $data["t_tempatlahirmember"] : null;
        $this->t_hubunganmember = !empty($data["t_hubunganmember"]) ? $data["t_hubunganmember"] : null;
        $this->t_jeniskelaminmember = !empty($data["t_jeniskelaminmember"]) ? $data["t_jeniskelaminmember"] : null;
        $this->t_namamember = !empty($data["t_namamember"]) ? $data["t_namamember"] : null;
        $this->t_noregistrasimember = !empty($data["t_noregistrasimember"]) ? $data["t_noregistrasimember"] : null;
        $this->t_wilayahmember = !empty($data["t_wilayahmember"]) ? $data["t_wilayahmember"] : null;
        $this->t_urutakmember = !empty($data["t_urutakmember"]) ? $data["t_urutakmember"] : null;
        $this->t_urutkkmember = !empty($data["t_urutkkmember"]) ? $data["t_urutkkmember"] : null;
        $this->t_idmember = !empty($data["t_idmember"]) ? $data["t_idmember"] : null;        
    }
    
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }
    
    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }
    
    public function getInputFilter()
    {
        $inputFilter = new InputFilter();
        
        $inputFilter->add([
            'name' => 't_idmember',
            'required' => FALSE,
        ]);
        
        $inputFilter->add([
            'name' => 't_urutkkmember',
            'required' => FALSE,
        ]);
        
        $inputFilter->add([
            'name' => 't_urutakmember',
            'required' => FALSE,
        ]);
        
        $inputFilter->add([
            'name' => 't_wilayahmember',
            'required' => TRUE,
        ]);
        
        $inputFilter->add([
            'name' => 't_noregistrasimember',
            'required' => TRUE,
            'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'min' => 9,
                        'max' => 9
                    ]
                ]
            ]
        ]);
        
        $inputFilter->add([
            'name' => 't_namamember',
            'required' => TRUE,
            'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'min' => 3,
                        'max' => 150
                    ]
                ]
            ]
        ]);
        
        $inputFilter->add([
            'name' => 't_jeniskelaminmember',
            'required' => TRUE,            
        ]);
        
        $inputFilter->add([
            'name' => 't_hubunganmember',
            'required' => TRUE,
        ]);
        
        $inputFilter->add([
            'name' => 't_tempatlahirmember',
            'required' => TRUE,
            'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'min' => 3,
                        'max' => 150
                    ]
                ]
            ]
        ]);
        
        $inputFilter->add([
            'name' => 't_tanggallahirmember',
            'required' => TRUE,
            'validators' => [
                [
                    'name' => Date::class,
                    'options' => [
                        'format' => 'd-m-Y'
                    ]
                ]
            ]
        ]);
        
        $inputFilter->add([
            'name' => 't_alamatjalanmember',
            'required' => TRUE,
            'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'min' => 3,
                    ]
                ]
            ]
        ]);
        
        $inputFilter->add([
            'name' => 't_alamatrtmember',
            'required' => TRUE,
            'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'min' => 3,
                        'max' => 3
                    ]
                ]
            ]
        ]);
        
        $inputFilter->add([
            'name' => 't_alamatrwmember',
            'required' => TRUE,
            'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'min' => 3,
                        'max' => 3
                    ]
                ]
            ]
        ]);
        
        $inputFilter->add([
            'name' => 't_alamatkelurahanmember',
            'required' => TRUE,
            'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'min' => 3,
                        'max' => 150
                    ]
                ]
            ]
        ]);
        
        $inputFilter->add([
            'name' => 't_alamatkecamatanmember',
            'required' => TRUE,
            'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'min' => 3,
                        'max' => 150
                    ]
                ]
            ]
        ]);
        
        $inputFilter->add([
            'name' => 't_alamatkotamember',
            'required' => TRUE,
            'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'min' => 3,
                        'max' => 150
                    ]
                ]
            ]
        ]);
        
        $inputFilter->add([
            'name' => 't_kodeposmember',
            'required' => FALSE,
            'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'min' => 5,
                        'max' => 5
                    ]
                ],
                [
                    'name' => Digits::class
                ]
            ]
        ]);
        
        $inputFilter->add([
            'name' => 't_telprumahmember',
            'required' => FALSE,
            'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'min' => 3,
                        'max' => 16
                    ]
                ],
                [
                    'name' => Digits::class
                ]
            ]
        ]);
        
        $inputFilter->add([
            'name' => 't_telphpmember',
            'required' => FALSE,
            'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'min' => 3,
                        'max' => 18
                    ]
                ],
                [
                    'name' => Digits::class
                ]
            ]
        ]);
        
        $inputFilter->add([
            'name' => 't_emailmember',
            'required' => FALSE,
            'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'min' => 3,
                        'max' => 150
                    ]
                ],
                [
                    'name' => EmailAddress::class
                ]
            ]
        ]);
        
        $inputFilter->add([
            'name' => 't_pendidikanmember',
            'required' => FALSE,
            'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'min' => 3,
                        'max' => 150
                    ]
                ]
            ]
        ]);
        
        $inputFilter->add([
            'name' => 't_pekerjaanmember',
            'required' => FALSE,
            'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'min' => 3,
                        'max' => 150
                    ]
                ]
            ]
        ]);
        
        $inputFilter->add([
            'name' => 't_perusahaanmember',
            'required' => FALSE,
            'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'min' => 3,
                        'max' => 150
                    ]
                ]
            ]
        ]);
        
        $inputFilter->add([
            'name' => 't_perusahaanalamatmember',
            'required' => FALSE,
        ]);
        
        $inputFilter->add([
            'name' => 't_telperusahaanmember',
            'required' => FALSE,
            'validators' => [
                [
                    'name' => Digits::class
                ]
            ]
        ]);
        
        $inputFilter->add([
            'name' => 't_tempatbaptismember',
            'required' => FALSE,
        ]);
        
        $inputFilter->add([
            'name' => 't_tanggalbaptismember',
            'required' => FALSE,
            'validators' => [
                [
                    'name' => Date::class,
                    'options' => [
                        'format' => 'd-m-Y'
                    ]
                ]
            ]
        ]);
        
        $inputFilter->add([
            'name' => 't_tempatsidimember',
            'required' => FALSE,
        ]);
        
        $inputFilter->add([
            'name' => 't_tanggalsidimember',
            'required' => FALSE,
            'validators' => [
                [
                    'name' => Date::class,
                    'options' => [
                        'format' => 'd-m-Y'
                    ]
                ]
            ]
        ]);
        
        $inputFilter->add([
            'name' => 't_tempatnikahmember',
            'required' => FALSE,
        ]);
        
        $inputFilter->add([
            'name' => 't_tanggalnikahmember',
            'required' => FALSE,
            'validators' => [
                [
                    'name' => Date::class,
                    'options' => [
                        'format' => 'd-m-Y'
                    ]
                ]
            ]
        ]);
        
        $inputFilter->add([
            'name' => 't_keteranganmember',
            'required' => FALSE,
        ]);
        
        
        $this->inputFilter = $inputFilter;
        return $this->inputFilter;
    }
}