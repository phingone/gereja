<?php
namespace Gereja\Model\Base\Setting;

use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\InputFilter\InputFilter;
use Zend\Validator\StringLength;

class WilayahBase implements InputFilterAwareInterface
{

    public $s_idwilayah;
    public $s_kodewilayah;
    public $s_namawilayah;
    public $s_keteranganwilayah;
    
    public $direction;
    public $page;
    public $rows;
    public $offset;
    protected  $inputFilter;
    
    public function exchangeArray($data)
    {
        $this->s_idwilayah = !empty($data["s_idwilayah"]) ? $data["s_idwilayah"] : null;
        $this->s_kodewilayah = !empty($data["s_kodewilayah"]) ? $data["s_kodewilayah"] : null;
        $this->s_namawilayah = !empty($data["s_namawilayah"]) ? $data["s_namawilayah"] : null;
        $this->s_keteranganwilayah = !empty($data["s_keteranganwilayah"]) ? $data["s_keteranganwilayah"] : null;
        
        $this->direction = ! empty($data['direction']) ? $data['direction'] : NULL;
        $this->page = ! empty($data['page']) ? $data['page'] : NULL;
        $this->rows = ! empty($data['rows']) ? $data['rows'] : NULL;
        $this->offset = ! empty($data['offset']) ? $data['offset'] : NULL;
    }
    public function getArrayCopy() 
    {
        return get_object_vars($this);
    }
    
    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        $inputFilter = new InputFilter();
        
        $inputFilter->add([
            'name' => 's_idwilayah',
            'required' => FALSE,
        ]);
        
        $inputFilter->add([
            'name' => 's_kodewilayah',
            'required' => TRUE,
            'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'min' => 3,
                        'max' => 3
                    ]
                ]
            ]
        ]);
        
        $inputFilter->add([
            'name' => 's_namawilayah',
            'required' => TRUE,
            'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'min' => 3,
                        'max' => 200
                    ]
                ]
            ]
        ]);
        
        $inputFilter->add([
            'name' => 's_keteranganwilayah',
            'required' => FALSE,            
        ]);
        
        $this->inputFilter = $inputFilter;
        return $this->inputFilter;
    }
}