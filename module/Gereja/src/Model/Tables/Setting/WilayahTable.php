<?php
namespace Gereja\Model\Tables\Setting;

use Zend\Db\TableGateway\TableGateway;
use Gereja\Model\Base\Setting\WilayahBase;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Where;
use Zend\Db\ResultSet\ResultSet;

class WilayahTable
{

    private $tableGateway;

    public function __construct(TableGateway $interface)
    {
        $this->tableGateway = $interface;
    }
    
    public function getById(WilayahBase $model)
    {
        $row = $this->tableGateway->select([
            's_idwilayah' => $model->s_idwilayah
        ]);
        return $row->current();
    }

    public function getByKode(WilayahBase $model)
    {
        $row = $this->tableGateway->select([
            's_kodewilayah' => $model->s_kodewilayah
        ]);
        return $row->current();
    }

    public function countByKode(WilayahBase $model)
    {
        $row = $this->tableGateway->select([
            's_kodewilayah' => $model->s_kodewilayah
        ]);
        return $row->count();
    }
    
    public function save(WilayahBase $model)
    {
        $data = [
            's_kodewilayah' => $model->s_kodewilayah,
            's_namawilayah' => $model->s_namawilayah,
            's_keteranganwilayah' => $model->s_keteranganwilayah
        ];
        $this->tableGateway->insert($data);
    }

    public function ubah(WilayahBase $model)
    {
        $data = [
            's_kodewilayah' => $model->s_kodewilayah,
            's_namawilayah' => $model->s_namawilayah,
            's_keteranganwilayah' => $model->s_keteranganwilayah
        ];
        $this->tableGateway->update($data, [
            's_idwilayah' => $model->s_idwilayah
        ]);
    }
    
    public function hapus(WilayahBase $model)
    {
        $this->tableGateway->delete([
            's_idwilayah' => $model->s_idwilayah
        ]);
    }
    
    public function gridCount(WilayahBase $model)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $Select = $sql->select();
        $Select->from('s_wilayah');
        $Where = new Where();
        $Select->where($Where);
        $Execute = $sql->prepareStatementForSqlObject($Select)->execute();
        return $Execute->count();
    }
    
    public function gridData(WilayahBase $model)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $Select = $sql->select();
        $Select->from('s_wilayah');
        $Where = new Where();
        $Select->where($Where);
        $Execute = $sql->prepareStatementForSqlObject($Select)->execute();
        $resultSet = new ResultSet();
        $result = $resultSet->initialize($Execute);
        return $result;
    }
    
    public function comboWilayah()
    {
        $data = $this->tableGateway->select();
        $selectData = [];
        foreach ($data as $row)
        {
            $selectData[$row->s_idwilayah] = "(".$row->s_kodewilayah.") ".$row->s_namawilayah;
        }
        return $selectData;
    }
}