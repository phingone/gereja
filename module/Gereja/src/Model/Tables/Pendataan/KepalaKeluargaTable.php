<?php
namespace Gereja\Model\Tables\Pendataan;

use Zend\Db\TableGateway\TableGateway;
use Gereja\Model\Base\Pendataan\KepalaKeluargaBase;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Where;
use Zend\Db\ResultSet\ResultSet;

class KepalaKeluargaTable 
{
    private $tableGateway;
    
    public function __construct(TableGateway $interface)
    {
        $this->tableGateway = $interface;
    }
    
    public function gridCount(KepalaKeluargaBase $model)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $Select = $sql->select();
        $Select->from('t_member');
        $Where = new Where();
        $Where->equalTo("t_iskkmember", "1");
        $Select->where($Where);
        $Execute = $sql->prepareStatementForSqlObject($Select)->execute();
        return $Execute->count();
    }
    
    public function gridData(KepalaKeluargaBase $model)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $Select = $sql->select();
        $Select->from('t_member');
        $Where = new Where();
        $Where->equalTo("t_iskkmember", "1");
        $Select->where($Where);
        $Execute = $sql->prepareStatementForSqlObject($Select)->execute();
        $resultSet = new ResultSet();
        $result = $resultSet->initialize($Execute);
        return $result;
    }
    
    public function getlasturutkk(KepalaKeluargaBase $model)
    {
        $select = $this->tableGateway->getSql()->select();
        $select->columns([
           't_urutkkmember' => new Expression("Max(t_urutkkmember)") 
        ]);
        $rowSet = $this->tableGateway->selectWith($select);
        $row = $rowSet->current();
        return $row;
    }
    
    public function getid(KepalaKeluargaBase $model) 
    {
        $row = $this->tableGateway->select([
            "t_idmember" => $model->t_idmember
        ]);    
        return $row->current();
    }
    
    public function simpan(KepalaKeluargaBase $model)
    {
        if(!is_null($model->t_tanggalbaptismember)){
            $model->t_tanggalbaptismember = date('Y-m-d', strtotime($model->t_tanggalbaptismember));
        }
        if(!is_null($model->t_tanggalsidimember)){
            $model->t_tanggalsidimember = date('Y-m-d', strtotime($model->t_tanggalsidimember));
        }
        if(!is_null($model->t_tanggalnikahmember)){
            $model->t_tanggalnikahmember = date('Y-m-d', strtotime($model->t_tanggalnikahmember));
        }
        $data = [
            't_urutkkmember'            => $model->t_urutkkmember,
            't_urutakmember'            => $model->t_urutakmember,
            't_wilayahmember'           => $model->t_wilayahmember,
            't_noregistrasimember'      => $model->t_noregistrasimember,
            't_namamember'              => $model->t_namamember,
            't_jeniskelaminmember'      => $model->t_jeniskelaminmember,
            't_hubunganmember'          => $model->t_hubunganmember,
            't_tempatlahirmember'       => $model->t_tempatlahirmember,
            't_tanggallahirmember'      => date('Y-m-d', strtotime($model->t_tanggallahirmember)),
            't_alamatjalanmember'       => $model->t_alamatjalanmember,
            't_alamatrtmember'          => $model->t_alamatrtmember,
            't_alamatrwmember'          => $model->t_alamatrwmember,
            't_alamatkelurahanmember'   => $model->t_alamatkelurahanmember,
            't_alamatkecamatanmember'   => $model->t_alamatkecamatanmember,
            't_alamatkotamember'        => $model->t_alamatkotamember,
            't_kodeposmember'           => $model->t_kodeposmember,
            't_telprumahmember'         => $model->t_telprumahmember,
            't_telphpmember'            => $model->t_telphpmember,
            't_emailmember'             => $model->t_emailmember,
            't_pendidikanmember'        => $model->t_pendidikanmember,
            't_pekerjaanmember'         => $model->t_pekerjaaanmember,
            't_perusahaanmember'        => $model->t_perusahaanmember,
            't_perusahaanalamatmember'  => $model->t_perusahaanalamatmember,
            't_telpperusahaanmember'    => $model->t_telpperusahaanmember,
            't_tempatbaptismember'      => $model->t_tempatbaptismember,
            't_tanggalbaptismember'     => $model->t_tanggalbaptismember,
            't_tempatsidimember'        => $model->t_tempatsidimember,
            't_tanggalsidimember'       => $model->t_tanggalsidimember,
            't_tempatnikahmember'       => $model->t_tempatnikahmember,
            't_tanggalnikahmember'      => $model->t_tanggalnikahmember,
            't_keteranganmember'        => $model->t_keteranganmember,
            't_iskkmember'              => 1
        ];
        $this->tableGateway->insert($data);
    }
    
    public function ubah(KepalaKeluargaBase $model)
    {
        if(!is_null($model->t_tanggalbaptismember)){
            $model->t_tanggalbaptismember = date('Y-m-d', strtotime($model->t_tanggalbaptismember));
        }
        if(!is_null($model->t_tanggalsidimember)){
            $model->t_tanggalsidimember = date('Y-m-d', strtotime($model->t_tanggalsidimember));
        }
        if(!is_null($model->t_tanggalnikahmember)){
            $model->t_tanggalnikahmember = date('Y-m-d', strtotime($model->t_tanggalnikahmember));
        }
        $data = [
            't_urutkkmember'            => $model->t_urutkkmember,
            't_urutakmember'            => $model->t_urutakmember,
            't_wilayahmember'           => $model->t_wilayahmember,
            't_noregistrasimember'      => $model->t_noregistrasimember,
            't_namamember'              => $model->t_namamember,
            't_jeniskelaminmember'      => $model->t_jeniskelaminmember,
            't_hubunganmember'          => $model->t_hubunganmember,
            't_tempatlahirmember'       => $model->t_tempatlahirmember,
            't_tanggallahirmember'      => date('Y-m-d', strtotime($model->t_tanggallahirmember)),
            't_alamatjalanmember'       => $model->t_alamatjalanmember,
            't_alamatrtmember'          => $model->t_alamatrtmember,
            't_alamatrwmember'          => $model->t_alamatrwmember,
            't_alamatkelurahanmember'   => $model->t_alamatkelurahanmember,
            't_alamatkecamatanmember'   => $model->t_alamatkecamatanmember,
            't_alamatkotamember'        => $model->t_alamatkotamember,
            't_kodeposmember'           => $model->t_kodeposmember,
            't_telprumahmember'         => $model->t_telprumahmember,
            't_telphpmember'            => $model->t_telphpmember,
            't_emailmember'             => $model->t_emailmember,
            't_pendidikanmember'        => $model->t_pendidikanmember,
            't_pekerjaanmember'         => $model->t_pekerjaaanmember,
            't_perusahaanmember'        => $model->t_perusahaanmember,
            't_perusahaanalamatmember'  => $model->t_perusahaanalamatmember,
            't_telpperusahaanmember'    => $model->t_telpperusahaanmember,
            't_tempatbaptismember'      => $model->t_tempatbaptismember,
            't_tanggalbaptismember'     => $model->t_tanggalbaptismember,
            't_tempatsidimember'        => $model->t_tempatsidimember,
            't_tanggalsidimember'       => $model->t_tanggalsidimember,
            't_tempatnikahmember'       => $model->t_tempatnikahmember,
            't_tanggalnikahmember'      => $model->t_tanggalnikahmember,
            't_keteranganmember'        => $model->t_keteranganmember,
            't_iskkmember'              => 1
        ];
        $this->tableGateway->update($data,[
            "t_idmember" => $model->t_idmember
        ]);
    }
    
    public function hapus(KepalaKeluargaBase $model){
        $this->tableGateway->delete([
            "t_idmember" => $model->t_idmember
        ]);
    }
    
}